Patterns
========

The GNOME design patterns represent the elements from which an overall design can be constructed. Some patterns are common to all apps, whereas others can be used selectively.

In many cases, a pattern corresponds to a single user interface widget or API. However, patterns can also consist of multiple widgets used together in a particular way.

The patterns are divided into four categories:

.. toctree::
   :hidden:

   patterns/containers
   patterns/nav
   patterns/controls
   patterns/feedback

.. cssclass:: tiled-toc

*  .. image:: /img/tiles/containers-windows.svg
      :target: patterns/containers.html
      :class: only-light
   .. image:: /img/tiles/containers-windows-dark.svg
      :target: patterns/containers.html
      :class: only-dark
      
   :doc:`Containers <patterns/containers>` 

*  .. image:: /img/tiles/nav-browsing.svg
      :target: patterns/nav.html
      :class: only-light
   .. image:: /img/tiles/nav-browsing-dark.svg
      :target: patterns/nav.html
      :class: only-dark

   :doc:`Navigation <patterns/nav>`

*  .. image:: /img/tiles/controls-switches.svg
      :target: patterns/controls.html
      :class: only-light
   .. image:: /img/tiles/controls-switches-dark.svg
      :target: patterns/controls.html
      :class: only-dark

   :doc:`Controls <patterns/controls>`

*  .. image:: /img/tiles/feedback-notifications.svg
      :target: patterns/feedback.html
      :class: only-light
   .. image:: /img/tiles/feedback-notifications-dark.svg
      :target: patterns/feedback.html
      :class: only-dark

   :doc:`Feedback <patterns/feedback>`
