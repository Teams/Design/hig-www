.. image:: img/hig.svg

GNOME Human Interface Guidelines
================================

The GNOME Human Interface Guidelines are the primary source of design documentation for those creating software with the GNOME development platform. They are primarily intended for app designers and developers, but are relevant to anyone wanting to familiarize themselves with GNOME UX.

Platform Definition
-------------------

The HIG is intended to be used in conjunction with recent versions of the GNOME platform, in particular GTK 4 and Libadwaita.

Content Overview
----------------

The HIG is made up of the following sections:

* :doc:`Design principles <principles>`: basic design rules and goals for the GNOME platform. This is the best place to start for anyone who is new to the HIG or GNOME design.
* :doc:`Resources <resources>`: an overview of the tools and assets that are available for GNOME design work.
* :doc:`Guidelines <guidelines>`: the standard conventions that are used in GNOME UX design, including how to write text, use icons, create app identities, and handle different types of input.
* :doc:`Patterns <patterns>`: covers the elements from which designs can be composed, such as windows, buttons, notifications or view switchers. The patterns are organized into four types: :doc:`containers </patterns/containers>`, :doc:`navigation </patterns/nav>`, :doc:`feedback </patterns/feedback>`, and :doc:`controls </patterns/controls>`.
* :doc:`Reference <reference>`: standard keyboard shortcuts and UI colors.

Contribute
----------

The `HIG project <https://gitlab.gnome.org/Teams/Websites/developer.gnome.org-hig>`_ on GNOME's GitLab instance can be used to report issues and propose changes.

.. toctree::
   :maxdepth: 1
   :hidden:

   principles
   resources
   guidelines
   patterns
   reference

